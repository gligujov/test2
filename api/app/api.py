from fastapi import FastAPI
from fastapi.responses import HTMLResponse, FileResponse
import pandas as pd
import uvicorn
import random as rnd
from pathlib import Path
from os import listdir
from os.path import isfile, join

app = FastAPI()
html_file_path = "api/app/static/index.html"

@app.get("/", response_class=HTMLResponse)
def root():
    with open(html_file_path, 'r') as file:
        content= file.read()
    return HTMLResponse(content=content,status_code=200)

@app.get("/generate/{filename}/{number}")
def generate(filename:str,number:int):
    with open(f"api/app/data/{filename}.txt",'w') as f:
        for i in range(number):
            f.write(str(rnd.randint(1, 10))+"\n")

@app.get("/getfiles/")
def all_subjects():
    onlyfiles = [f for f in listdir("api/app/data/") if isfile(join("api/app/data/", f))]
    return onlyfiles

@app.get("/getfile/{filename}")
def subject(filename:str):
    file_path = f"api/app/data/{filename}.txt"
    return FileResponse(file_path, media_type='text/plain', filename=file_path)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5050)
